﻿using System;
using System.Data.Entity;
using Hrms.Domain.Entities;
using Hrms.Persistence.Context;
using Hrms.Persistence.Repositories;

namespace Hrms.Persistence.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork, IDisposable
    {
        #region Fields

        private readonly DbContext _context;
        private bool _disposed;
        private readonly Lazy<IRepository<Employee>> _employeeRepository;
        private readonly Lazy<IRepository<EmployeeTenure>> _employeeTenureRepository;
        private readonly Lazy<IRepository<Department>> _departmentRepository;
        private readonly Lazy<IRepository<Team>> _teamRepository;
        private readonly Lazy<IRepository<Location>> _locationRepository;
        private readonly Lazy<IRepository<DepartmentTeamLocation>> _departmentTeamLocationRepository;

        #endregion

        #region Properties

        public IRepository<Employee> EmployeeRepository
        {
            get
            {
                return _employeeRepository.Value;
            }
        }

        public IRepository<EmployeeTenure> EmployeeTenureRepository
        {
            get
            {
                return _employeeTenureRepository.Value;
            }
        }

        public IRepository<Department> DepartmentRepository
        {
            get
            {
                return _departmentRepository.Value;
            }
        }

        public IRepository<Team> TeamRepository
        {
            get
            {
                return _teamRepository.Value;
            }
        }

        public IRepository<Location> LocationRepository
        {
            get
            {
                return _locationRepository.Value;
            }
        }

        public IRepository<DepartmentTeamLocation> DepartmentTeamLocationRepository
        {
            get
            {
                return _departmentTeamLocationRepository.Value;
            }
        }

        #endregion

        public UnitOfWork(HrmsContext context)
        {
            _context = context as DbContext;
            _employeeRepository = new Lazy<IRepository<Employee>>(() => new GenericRepository<Employee>(context));
            _employeeTenureRepository = new Lazy<IRepository<EmployeeTenure>>(() => new GenericRepository<EmployeeTenure>(context));
            _departmentRepository = new Lazy<IRepository<Department>>(() => new GenericRepository<Department>(context));
            _teamRepository = new Lazy<IRepository<Team>>(() => new GenericRepository<Team>(context));
            _locationRepository = new Lazy<IRepository<Location>>(() => new GenericRepository<Location>(context));
            _departmentTeamLocationRepository = new Lazy<IRepository<DepartmentTeamLocation>>(() => new GenericRepository<DepartmentTeamLocation>(context));
        }

        public void CommitUnitOfWork()
        {
            _context.SaveChanges();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool isDisposing)
        {
            if (!_disposed)
            {
                if (isDisposing)
                {
                    _context.Dispose();
                }
            }
            _disposed = true;
        }
    }
}
