﻿namespace Hrms.Persistence.UnitOfWork
{
    public interface IUnitOfWork
    {
        void CommitUnitOfWork();
    }
}
