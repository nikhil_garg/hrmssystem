﻿using System.Data.Entity;
using Hrms.Domain.Entities;

namespace Hrms.Persistence.Context
{
    public class HrmsContext : DbContext
    {
        public virtual IDbSet<Employee> Employees { get; set; }
        public virtual IDbSet<Department> Departments { get; set; }
        public virtual IDbSet<Team> Teams { get; set; }
        public virtual IDbSet<Location> Locations { get; set; }
        public virtual IDbSet<EmployeeTenure> EmployeeTenures { get; set; }
        public virtual IDbSet<DepartmentTeamLocation> DepartmentTeamLocations { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<EmployeeTenure>()
                .HasRequired(et => et.DepartmentTeamLocation)
                .WithMany()
                .WillCascadeOnDelete(false);

            base.OnModelCreating(modelBuilder);
        }
    }
}
