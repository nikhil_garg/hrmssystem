﻿using System.Linq;

namespace Hrms.Persistence.Repositories
{
    public interface IRepository<T> where T : class
    {
        IQueryable<T> GetAll();
        T GetById(object id);
        void Add(T obj);
        void Update(T obj);
        void Delete(object id);
        void Delete(T entityToDelete);
    }
}
