﻿using System.Data.Entity;
using System.Linq;
using Hrms.Persistence.Context;

namespace Hrms.Persistence.Repositories
{
    public class GenericRepository<T> : IRepository<T> where T : class
    {
        private readonly DbContext _context;
        private readonly DbSet<T> _dbSet;

        public GenericRepository(HrmsContext context)
        {
            _context = context as DbContext;
            if (_context != null)
            {
                _dbSet = _context.Set<T>();
            }
        }

        public virtual IQueryable<T> GetAll()
        {
            return from set in _dbSet
                select set;
        }

        public virtual T GetById(object id)
        {
            return _dbSet.Find(id);
        }

        public virtual void Add(T obj)
        {
            _dbSet.Add(obj);
        }

        public virtual void Update(T obj)
        {
            _dbSet.Attach(obj);
            _context.Entry(obj).State = EntityState.Modified;
        }

        public virtual void Delete(object id)
        {
            T entityToDelete = _dbSet.Find(id);
            Delete(entityToDelete);
        }

        public virtual void Delete(T entityToDelete)
        {
            if (_context.Entry(entityToDelete).State == EntityState.Detached)
            {
                _dbSet.Attach(entityToDelete);
            }
            _dbSet.Remove(entityToDelete);
        }
    }
}
