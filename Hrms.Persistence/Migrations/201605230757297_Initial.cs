namespace Hrms.Persistence.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Departments",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        HeadId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Employees", t => t.HeadId, cascadeDelete: true)
                .Index(t => t.HeadId);
            
            CreateTable(
                "dbo.Employees",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(nullable: false, maxLength: 600),
                        FirstName = c.String(nullable: false, maxLength: 300),
                        LastName = c.String(nullable: false, maxLength: 300),
                        DateOfBirth = c.DateTime(nullable: false, storeType: "date"),
                        Gender = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.EmployeeTenures",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        EmpId = c.Guid(nullable: false),
                        DtlId = c.Guid(nullable: false),
                        DateOfJoining = c.DateTime(nullable: false),
                        DateOfLeaving = c.DateTime(nullable: false),
                        Designation = c.Int(nullable: false),
                        AnnualSalary = c.Single(nullable: false),
                        CurrencyUnit = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DepartmentTeamLocations", t => t.DtlId)
                .ForeignKey("dbo.Employees", t => t.EmpId, cascadeDelete: true)
                .Index(t => t.EmpId)
                .Index(t => t.DtlId);
            
            CreateTable(
                "dbo.DepartmentTeamLocations",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        DeptId = c.Guid(nullable: false),
                        TeamId = c.Guid(nullable: false),
                        LocationId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Departments", t => t.DeptId, cascadeDelete: true)
                .ForeignKey("dbo.Locations", t => t.LocationId, cascadeDelete: true)
                .ForeignKey("dbo.Teams", t => t.TeamId, cascadeDelete: true)
                .Index(t => t.DeptId)
                .Index(t => t.TeamId)
                .Index(t => t.LocationId);
            
            CreateTable(
                "dbo.Locations",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        City = c.String(),
                        State = c.String(),
                        Country = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Teams",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ShortName = c.String(),
                        LongName = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Departments", "HeadId", "dbo.Employees");
            DropForeignKey("dbo.EmployeeTenures", "EmpId", "dbo.Employees");
            DropForeignKey("dbo.EmployeeTenures", "DtlId", "dbo.DepartmentTeamLocations");
            DropForeignKey("dbo.DepartmentTeamLocations", "TeamId", "dbo.Teams");
            DropForeignKey("dbo.DepartmentTeamLocations", "LocationId", "dbo.Locations");
            DropForeignKey("dbo.DepartmentTeamLocations", "DeptId", "dbo.Departments");
            DropIndex("dbo.DepartmentTeamLocations", new[] { "LocationId" });
            DropIndex("dbo.DepartmentTeamLocations", new[] { "TeamId" });
            DropIndex("dbo.DepartmentTeamLocations", new[] { "DeptId" });
            DropIndex("dbo.EmployeeTenures", new[] { "DtlId" });
            DropIndex("dbo.EmployeeTenures", new[] { "EmpId" });
            DropIndex("dbo.Departments", new[] { "HeadId" });
            DropTable("dbo.Teams");
            DropTable("dbo.Locations");
            DropTable("dbo.DepartmentTeamLocations");
            DropTable("dbo.EmployeeTenures");
            DropTable("dbo.Employees");
            DropTable("dbo.Departments");
        }
    }
}
