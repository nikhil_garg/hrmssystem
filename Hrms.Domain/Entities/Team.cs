﻿using System;

namespace Hrms.Domain.Entities
{
    public class Team
    {
        public Guid Id { get; set; }
        public string ShortName { get; set; }
        public string LongName { get; set; }
    }
}
