﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hrms.Domain.Entities
{
    public class Department
    {
        public Guid Id { get; set; }
        public string Name { get; set; }

        [ForeignKey("Head")]
        public Guid HeadId { get; set; }

        [Required]
        public Employee Head { get; set; }
    }
}
