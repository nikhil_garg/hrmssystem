﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hrms.Domain.Entities
{
    public class DepartmentTeamLocation
    {
        public Guid Id { get; set; }

        [ForeignKey("Department")]
        public Guid DeptId { get; set; }

        [Required]
        public Department Department { get; set; }

        [ForeignKey("Team")]
        public Guid TeamId { get; set; }

        [Required]
        public Team Team { get; set; }

        [ForeignKey("Location")]
        public Guid LocationId { get; set; }

        [Required]
        public Location Location { get; set; }
    }
}
