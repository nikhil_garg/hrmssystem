﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Hrms.Domain.Enumerations;

namespace Hrms.Domain.Entities
{
    public class EmployeeTenure
    {
        public Guid Id { get; set; }

        [ForeignKey("Employee")]
        public Guid EmpId { get; set; }

        [Required]
        public Employee Employee { get; set; }

        [ForeignKey("DepartmentTeamLocation")]
        public Guid DtlId { get; set; }

        [Required]
        public DepartmentTeamLocation DepartmentTeamLocation { get; set; }

        public DateTime DateOfJoining { get; set; }
        public DateTime DateOfLeaving { get; set; }
        public Designation Designation { get; set; }
        public float AnnualSalary { get; set; }
        public Currency CurrencyUnit { get; set; }
    }
}
