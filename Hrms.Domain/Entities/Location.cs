﻿using System;

namespace Hrms.Domain.Entities
{
    public class Location
    {
        public Guid Id { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
    }
}
