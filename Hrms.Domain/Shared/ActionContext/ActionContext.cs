﻿using System.Collections.Generic;
using System.Linq;

namespace Hrms.Domain.Shared.ActionContext
{
    public class ActionContext<TRequest,TResponse> 
    {
        #region Properties

        public TRequest Req { get; set; }
        public Errors ErrorResult { get; set; }
        public Validations ValidationResult { get; set; }
        public TResponse Res { get; set; }

        #endregion Properties

        #region Constructor

        private ActionContext(TRequest req, TResponse res)
        {
            Req = req;
            ErrorResult = new Errors();
            ValidationResult = new Validations();
            Res = res;
        }

        #endregion Constructor

        #region Methods

        public static ActionContext<TRequest,TResponse> GetInstance(TRequest req, TResponse res)
        {
            return new ActionContext<TRequest,TResponse>(req, res);
        }

        /// <summary>
        /// Returns the string of all validation result message.
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public static IEnumerable<string> GetMessage(ActionContext<TRequest, TResponse> context)
        {
            var errorStatus = context.ErrorResult != null && context.ErrorResult.Get().Count > 0;
            var validationStatus = context.ValidationResult != null && context.ValidationResult.Get().Count > 0;
            if(errorStatus)
            {
                var errorResult = context.ErrorResult.Get();
                return errorResult.Select(errorParameter => string.Format("{0}:{1}", errorParameter.ErrorCode, errorParameter.ErrorMessage)).ToList();
            }
            else if (validationStatus)
            {
                var validationResult = context.ValidationResult.Get();
                return validationResult.Select(validationParameter => validationParameter.Message).ToList();
            }
            return null;
        }

        #endregion Methods
    }
}
