﻿using System;

namespace Hrms.Domain.Shared.ActionContext
{
    public class ErrorsParameter
    {
        public string ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
        public Enum EnumStatus { get; set; }
    }
}
