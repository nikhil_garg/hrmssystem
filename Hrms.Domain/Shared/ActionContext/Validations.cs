﻿using System.Collections.Generic;

namespace Hrms.Domain.Shared.ActionContext
{
    public class Validations
    {
        private IList<ValidationsParameter> ValidationContextParams { get; set; }

        public Validations()
        {
           ValidationContextParams = new List<ValidationsParameter>();
        }

        public void Add(ValidationsParameter parameter)
        {
            ValidationContextParams.Add(parameter);
        }

        public IList<ValidationsParameter> Get()
        {
            return ValidationContextParams;
        }
    }
}
