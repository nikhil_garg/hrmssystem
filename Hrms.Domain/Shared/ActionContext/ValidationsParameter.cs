﻿using System;

namespace Hrms.Domain.Shared.ActionContext
{
    public class ValidationsParameter
    {
        public Enum EnumStatus { get; set; }
        public string ColumnName { get; set; }
        public string ColumnValue { get; set; }        
        public string Message { get; set; }
    }
}
