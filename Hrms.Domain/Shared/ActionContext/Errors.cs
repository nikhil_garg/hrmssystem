﻿using System;
using System.Collections.Generic;

namespace Hrms.Domain.Shared.ActionContext
{
    public class Errors
    {
        private IList<ErrorsParameter> ErrorContextParams { get; set; }

        public Errors()
        {
            ErrorContextParams = new List<ErrorsParameter>();
        }

        public void Add(string errorCode, string errorMessage, Enum errorStatus)
        {
            var errorContextParameter = new ErrorsParameter
            {
                ErrorCode = errorCode,
                ErrorMessage = errorMessage,
                EnumStatus = errorStatus
            };
            ErrorContextParams.Add(errorContextParameter);
        }

        public IList<ErrorsParameter> Get()
        {
            return ErrorContextParams;
        }
    }
}
