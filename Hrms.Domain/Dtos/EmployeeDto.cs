﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hrms.Domain.Constants;
using Hrms.Domain.Entities;
using Hrms.Domain.Enumerations;

namespace Hrms.Domain.Dtos
{
    public class EmployeeDto
    {
        public Guid Id { get; set; }

        [Required]
        [StringLength(600, ErrorMessage = "Name length should be between 1 and 600 characters", MinimumLength = 1)]
        public string Name { get; set; }

        [Required]
        [StringLength(300, ErrorMessage = "First name length should be between 1 and 300 characters", MinimumLength = 1)]
        [RegularExpression(EmployeeConstants.NameRegex, ErrorMessage = "First name is not in correct format")]
        public string FirstName { get; set; }

        [Required]
        [StringLength(300, ErrorMessage = "Last name length should be between 1 and 300 characters", MinimumLength = 1)]
        [RegularExpression(EmployeeConstants.NameRegex, ErrorMessage = "Last name is not in correct format")]
        public string LastName { get; set; }

        public DateTime DateOfBirth { get; set; }

        public Gender Gender { get; set; }

        public virtual ICollection<EmployeeTenure> EmployeeTenures { get; set; }
    }
}
