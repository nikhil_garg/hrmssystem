﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Hrms.Domain.Dtos;
using Hrms.Domain.Employee.Add.Request;
using Hrms.Domain.Employee.Add.Response;
using Hrms.Domain.Shared.ActionContext;

namespace Hrms.Domain.Employee.Add.Validations
{
    public class DataAnnotationValidator : IEmployeeAddValidator
    {
        private List<ValidationResult> _validationResult;

        public void Validate(ActionContext<EmployeeAddRequest, EmployeeAddResponse> context)
        {
                var validationError = ExecuteDataAnnotations(context.Req.EmployeeDto);
                if (validationError.Length > 0)
                {
                    context.ValidationResult.Add(new ValidationsParameter {Message = validationError} );
                }
        }

        private string ExecuteDataAnnotations(EmployeeDto employeeDto)
        {
            var validationError = new StringBuilder();
            if (_validationResult == null)
                _validationResult = new List<ValidationResult>();
            else
                _validationResult.Clear();

            Validator.TryValidateObject(employeeDto, new ValidationContext(employeeDto, null, null), _validationResult, true);
            foreach (var result in _validationResult)
                validationError.Append($"{result} ");

            return validationError.ToString().TrimEnd();
        }
    }
}
