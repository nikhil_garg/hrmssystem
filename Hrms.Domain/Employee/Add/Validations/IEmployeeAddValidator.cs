﻿using Hrms.Domain.Employee.Add.Request;
using Hrms.Domain.Employee.Add.Response;
using Hrms.Domain.Shared.ActionContext;

namespace Hrms.Domain.Employee.Add.Validations
{
    public interface IEmployeeAddValidator
    {
        void Validate(ActionContext<EmployeeAddRequest, EmployeeAddResponse> context);
    }
}
