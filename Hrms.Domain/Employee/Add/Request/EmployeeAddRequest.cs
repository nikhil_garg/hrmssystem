﻿using Hrms.Domain.Dtos;

namespace Hrms.Domain.Employee.Add.Request
{
    public class EmployeeAddRequest
    {
        public EmployeeDto EmployeeDto { get; set; }
    }
}
