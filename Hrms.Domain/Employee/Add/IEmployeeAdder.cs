﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Hrms.Domain.Employee.Add.Request;
using Hrms.Domain.Employee.Add.Response;
using Hrms.Domain.Shared.ActionContext;

namespace Hrms.Domain.Employee.Add
{
    public interface IEmployeeAdder
    {
        void ValidateRequest(ActionContext<EmployeeAddRequest, EmployeeAddResponse> context);
    }
}
