﻿using System.Collections.Generic;
using Hrms.Domain.Employee.Add.Request;
using Hrms.Domain.Employee.Add.Response;
using Hrms.Domain.Employee.Add.Validations;
using Hrms.Domain.Shared.ActionContext;

namespace Hrms.Domain.Employee.Add
{
    public class EmployeeAdder : IEmployeeAdder
    {
        private readonly IEnumerable<IEmployeeAddValidator> _validators;

        public EmployeeAdder(IEnumerable<IEmployeeAddValidator> validators)
        {
            _validators = validators;
        }

        public void ValidateRequest(ActionContext<EmployeeAddRequest, EmployeeAddResponse> context)
        {
            foreach (var validator in _validators)
            {
                validator.Validate(context);
            }
        }
    }
}
