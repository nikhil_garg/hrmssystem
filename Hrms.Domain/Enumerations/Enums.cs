﻿
namespace Hrms.Domain.Enumerations
{
    public enum Gender
    {
        Male = 10,
        Female = 20,
        Unknown = 30
    }

    public enum Currency
    {
        USD = 10,
        INR = 20
    }

    public enum Designation
    {
        JuniorSoftwareEngineer = 10,
        SoftwareEngineer = 20,
        SeniorSoftwareEngineer = 30,
        LeadSoftwareEngineer = 40,
        PrincipalSoftwareEngineer = 50,
        JuniorTestEngineer = 60,
        TestEngineer = 70,
        SeniorTestEngineer = 80,
        LeadTestEngineer = 90
    }
}
