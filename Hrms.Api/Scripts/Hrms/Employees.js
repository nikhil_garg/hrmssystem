﻿$(document).ready(function() {

    //console.log(document.location.origin);
    //console.log(window.location.protocol + "//" + window.location.host + "/");
    //console.log(BASE_HREF);

    var onSuccess = function(data, status, xhr) {
        $('#employeesTable').DataTable({
            data: data,

            columns: [
                { title: "Name", data: "Name" },
                {
                    title: "Date of Birth",
                    data: "DateOfBirth",
                    render: function(dateOfBirth) {
                        return moment(dateOfBirth).format('MMMM Do YYYY');
                    }
                },
                {
                    title: "Gender",
                    data: "Gender",
                    render: function(gender) {
                        var returnVal = "Unknown";

                        if (gender == 10)
                            returnVal = "Male";
                        else if (gender == 20)
                            returnVal = "Female";

                        return returnVal;
                    }
                }
            
            ],

            bDestroy: true
        });
    };

    $.ajax({
        url: BASE_HREF + '/api/Employees',
        type: "GET",
        contentType: "application/json; charset=utf-8",
        success: onSuccess,
        error: function(xhr, status, error) {
            console.log(error);
        }
    });

});