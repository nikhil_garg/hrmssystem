﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AutoMapper.QueryableExtensions;
using Hrms.Api.Controllers.API.Extensions;
using Hrms.Api.Infrastructure.Mappings;
using Hrms.Api.Models;
using Hrms.Domain.Dtos;
using Hrms.Domain.Employee.Add;
using Hrms.Domain.Employee.Add.Request;
using Hrms.Domain.Employee.Add.Response;
using Hrms.Domain.Entities;
using Hrms.Domain.Shared.ActionContext;
using Hrms.Persistence.Context;
using Hrms.Persistence.UnitOfWork;

namespace Hrms.Api.Controllers.API
{
    public class EmployeesController : ApiController
    {
        private readonly HrmsContext _context;
        private readonly IEmployeeAdder _employeeAdder;

        public EmployeesController(HrmsContext context, IEmployeeAdder employeeAdder)
        {
            _context = context;
            _employeeAdder = employeeAdder;
        }

        // GET api/employees
        public IList<EmployeeParameters> Get()
        {
            var uow = new UnitOfWork(_context);

            return uow.EmployeeRepository.GetAll()
                .ProjectTo<EmployeeParameters>(AutoMappings.Config)
                .ToList();
        }

        // GET api/values/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/employees
        public HttpResponseMessage Post(EmployeeParameters employeeParameters)
        {
            var actionContext = ActionContext<EmployeeAddRequest, EmployeeAddResponse>.GetInstance(employeeParameters.ToEmployeeAddRequest(), new EmployeeAddResponse());
            _employeeAdder.ValidateRequest(actionContext);
            if (actionContext.ValidationResult.Get().Any())
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, actionContext.ValidationResult.Get());
            }

            using (var uow = new UnitOfWork(_context))
            {
                var emp = AutoMappings.Mapper.Map<EmployeeParameters, Employee>(employeeParameters);

                uow.EmployeeRepository.Add(emp);
                uow.CommitUnitOfWork();
            }

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}