﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Hrms.Api.Models;
using Hrms.Domain.Dtos;
using Hrms.Domain.Employee.Add.Request;

namespace Hrms.Api.Controllers.API.Extensions
{
    public static class EmployeeExtension
    {
        public static EmployeeAddRequest ToEmployeeAddRequest(this EmployeeParameters employeeParameters)
        {
            return new EmployeeAddRequest
            {
                EmployeeDto = new EmployeeDto
                            {
                                Id = employeeParameters.Id,
                                FirstName = employeeParameters.FirstName,
                                LastName = employeeParameters.LastName,
                                DateOfBirth = employeeParameters.DateOfBirth
                            }
            };
        }
    }
}