﻿using System.Collections.Generic;
using System.Globalization;
using System.Web.Mvc;
using Hrms.Domain.Enumerations;

namespace Hrms.Api.Controllers.UI
{
    public class EmployeesController : Controller
    { 
        //
        // GET: /Employees/

        public ActionResult Index()
        {
            return View();
        }

        //
        // GET: /Employees/Details/5

        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Employees/Create

        public ActionResult Create()
        {
            IList<SelectListItem> genderList = new List<SelectListItem>
            {
                new SelectListItem()
                {
                    Text = Gender.Male.ToString(),
                    Value = ((int) Gender.Male).ToString(CultureInfo.InvariantCulture),
                    Selected = true
                },
                new SelectListItem()
                {
                    Text = Gender.Female.ToString(),
                    Value = ((int) Gender.Female).ToString(CultureInfo.InvariantCulture)
                },
                new SelectListItem()
                {
                    Text = Gender.Unknown.ToString(),
                    Value = ((int) Gender.Unknown).ToString(CultureInfo.InvariantCulture)
                }
            };

            ViewBag.GenderOptions = genderList;

            return View();
        }

        //
        // POST: /Employees/Create

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // Ajax call to create new employee is fired using JQuery
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Employees/Edit/5

        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /Employees/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Employees/Delete/5

        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Employees/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
