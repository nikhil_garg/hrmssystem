﻿using System.Web.Mvc;

namespace Hrms.Api.Controllers.UI
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}
