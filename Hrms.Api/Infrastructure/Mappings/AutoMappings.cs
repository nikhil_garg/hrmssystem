﻿using System;
using AutoMapper;
using Hrms.Api.Models;
using Hrms.Domain.Entities;

namespace Hrms.Api.Infrastructure.Mappings
{
    public class AutoMappings
    {
        public static MapperConfiguration Config;

        public static IMapper Mapper;

        public static void SetUp()
        {
            Config = new MapperConfiguration(
                config =>
                {
                    config.CreateMap<Employee, EmployeeParameters>();

                    config.CreateMap<EmployeeParameters, Employee>()
                        .ForMember(dest => dest.Name, opt =>
                            opt.MapFrom(src => src.FirstName + " " + src.LastName))
                        .ForMember(dest => dest.Id, opt =>
                            opt.MapFrom(src => Guid.NewGuid()));
                }
                );

            Mapper = Config.CreateMapper();
        }
    }
}