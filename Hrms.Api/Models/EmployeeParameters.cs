﻿using System;
using System.ComponentModel.DataAnnotations;
using Hrms.Domain.Enumerations;

namespace Hrms.Api.Models
{
    public class EmployeeParameters
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Display(Name = "Date of Birth")]
        public DateTime DateOfBirth { get; set; }

        public Gender Gender { get; set; }
    }
}