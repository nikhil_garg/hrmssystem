﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hrms.Domain.Dtos;
using Hrms.Domain.Request;
using Hrms.Domain.Response;
using Hrms.Domain.Shared.ActionContext;

namespace Hrms.Service.Employee.Add.Validations
{
    public class DataAnnotationValidator : IEmployeeAddValidator
    {
        private List<ValidationResult> _validationResult;

        public void Validate(ActionContext<EmployeeAddRequest, EmployeeAddResponse> context)
        {
                var validationError = ExecuteDataAnnotations(context.Req.EmployeeDto);
                if (validationError.Length > 0)
                {
                    context.ValidationResult.Add(new ValidationsParameter {Message = validationError} );
                }
        }

        private string ExecuteDataAnnotations(EmployeeDto employeeDto)
        {
            var validationError = new StringBuilder();
            if (_validationResult == null)
                _validationResult = new List<ValidationResult>();
            else
                _validationResult.Clear();

            Validator.TryValidateObject(employeeDto, new ValidationContext(employeeDto, null, null), _validationResult, true);
            foreach (var result in _validationResult)
                validationError.Append($"{result} ");

            return validationError.ToString().TrimEnd();
        }
    }
}
