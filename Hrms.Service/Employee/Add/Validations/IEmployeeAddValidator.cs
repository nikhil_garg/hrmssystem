﻿using Hrms.Domain.Dtos;
using Hrms.Domain.Request;
using Hrms.Domain.Response;
using Hrms.Domain.Shared.ActionContext;

namespace Hrms.Service.Employee.Add.Validations
{
    public interface IEmployeeAddValidator
    {
        void Validate(ActionContext<EmployeeAddRequest, EmployeeAddResponse> context);
    }
}
